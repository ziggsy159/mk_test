# mk_test
Script for runinng test files on APS1 (Linux / WSL)

## Dependencies
- bc: for precise time calculation
- GNU Time: for measuring peak memory

## How to
Move test files into the same directory as Nalogax.java
```bash
$ mk_test # Runs all tests
$ mk_test 1 # Runs test 1 (Ix_1.txt)
$ mk_test --diff 1 # Shows diff between R1.txt and Ox_1.txt
```
